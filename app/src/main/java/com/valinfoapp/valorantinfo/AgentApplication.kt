package com.valinfoapp.valorantinfo

import android.app.Application
import androidx.room.Room
import com.valinfoapp.valorantinfo.data.database.AgentDatabase

class AgentApplication: Application() {
    companion object {
        lateinit var database: AgentDatabase
    }
    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(this,
            AgentDatabase::class.java,
            "AgentDatabase").build()
    }
}
