package com.valinfoapp.valorantinfo.data.model

data class Role(
    val uuid: String,
    val displayName: String,
    val description: String,
    val displayIcon: String
)