package com.valinfoapp.valorantinfo.data.api

import com.valinfoapp.valorantinfo.data.model.DetailData
import com.valinfoapp.valorantinfo.data.model.ListData
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {

    @GET("agents")
    fun getAgentsList(): Call<ListData>

    @GET("agents/{agentUuid}")
    fun getAgentDetail(@Path("agentUuid") agentUuid: String): Call<DetailData>

    companion object {
        private var retrofitService: ApiInterface? = null
        fun getInstance() : ApiInterface {
            if (retrofitService == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl("https://valorant-api.com/v1/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                retrofitService = retrofit.create(ApiInterface::class.java)
            }
            return retrofitService!!
        }
    }
}