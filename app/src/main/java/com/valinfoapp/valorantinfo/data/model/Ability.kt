package com.valinfoapp.valorantinfo.data.model


data class Ability(
    val displayName: String,
    val description: String,
    val displayIcon: String
)