package com.valinfoapp.valorantinfo.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.valinfoapp.valorantinfo.data.model.AgentEntity

@Dao
interface AgentDAO {
    @Query("SELECT * FROM AgentEntity")
    fun getAllAgents(): LiveData<MutableList<AgentEntity>>
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addAgent(contact: AgentEntity)
    @Update
    fun updateAgent(contact: AgentEntity)
    @Delete
    fun deleteAgent(contact: AgentEntity)
}


