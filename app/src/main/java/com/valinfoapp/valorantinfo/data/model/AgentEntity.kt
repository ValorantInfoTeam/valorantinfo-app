package com.valinfoapp.valorantinfo.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity (tableName = "AgentEntity")

data class AgentEntity(
    @PrimaryKey val uuid: String,
    val displayName: String,
    val description: String,
    val displayIcon: String,
    val fullPortrait: String,
    val role: Role,
    val abilities: ArrayList<Ability>,
    var fav: Boolean
)


