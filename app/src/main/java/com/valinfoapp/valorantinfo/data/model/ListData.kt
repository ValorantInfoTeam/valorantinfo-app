package com.valinfoapp.valorantinfo.data.model


data class ListData(
    val `data`: List<AgentEntity>
)