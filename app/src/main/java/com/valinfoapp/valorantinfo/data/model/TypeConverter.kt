package com.valinfoapp.valorantinfo.data.model

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONObject
import java.lang.reflect.Type

class TypeConverter {

    @TypeConverter
    fun fromString(value: String?): ArrayList<Ability?>? {
        val listType: Type = object : TypeToken<ArrayList<Ability?>?>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromArrayList(list: ArrayList<Ability?>?): String? {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun fromRole(role: Role): String {
        return JSONObject().apply {
            put("uuid", role.uuid)
            put("displayName", role.displayName)
            put("description", role.description)
            put("displayIcon", role.displayIcon)
        }.toString()
    }

    @TypeConverter
    fun toRole(role: String): Role {
        val json = JSONObject(role)
        return Role(
            json.getString("uuid"),
            json.getString("displayName"),
            json.getString("description"),
            json.getString("displayIcon")
        )
    }

    @TypeConverter
    fun fromAbility(ability: Ability): String {
        return JSONObject().apply {
            put("displayName", ability.displayName)
            put("description", ability.description)
            put("displayIcon", ability.displayIcon)
        }.toString()
    }

    @TypeConverter
    fun toAbility(ability: String): Ability {
        val json = JSONObject(ability)
        return Ability(
            json.getString("displayName"),
            json.getString("description"),
            json.getString("displayIcon")
        )
    }
}