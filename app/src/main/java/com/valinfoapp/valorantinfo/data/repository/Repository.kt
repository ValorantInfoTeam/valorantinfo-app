package com.valinfoapp.valorantinfo.data.repository

import androidx.lifecycle.LiveData
import com.valinfoapp.valorantinfo.AgentApplication
import com.valinfoapp.valorantinfo.data.api.ApiInterface
import com.valinfoapp.valorantinfo.data.model.AgentEntity

class Repository {
    private val apiService = ApiInterface.getInstance()
    private val agentDao = AgentApplication.database.agentDao()
    fun getAgentsList() = apiService.getAgentsList()
    fun getAgentDetail(id: String) = apiService.getAgentDetail(id)
    val mItemList: LiveData<MutableList<AgentEntity>> = agentDao.getAllAgents()
}