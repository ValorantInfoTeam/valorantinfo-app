package com.valinfoapp.valorantinfo.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.valinfoapp.valorantinfo.data.dao.AgentDAO
import com.valinfoapp.valorantinfo.data.model.AgentEntity
import com.valinfoapp.valorantinfo.data.model.TypeConverter

@Database(entities = [AgentEntity::class], version = 1)
@TypeConverters(TypeConverter::class)
abstract class AgentDatabase: RoomDatabase() {
   abstract fun agentDao(): AgentDAO
}
