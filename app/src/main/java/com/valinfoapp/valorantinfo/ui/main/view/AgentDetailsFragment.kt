package com.valinfoapp.valorantinfo.ui.main.view

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.valinfoapp.valorantinfo.ui.main.viewModel.AgentsViewModel
import com.valinfoapp.valorantinfo.R
import com.squareup.picasso.Picasso

class AgentDetailsFragment: Fragment(R.layout.agent_details_layout) {

    private val agentsViewModel: AgentsViewModel by activityViewModels()

    private lateinit var agentNameDetails: TextView

    private lateinit var agentFullImg: ImageView
    private lateinit var agentRoleInf: TextView
    private lateinit var agentRoleDesc: TextView
    private lateinit var agentDescInf: TextView
    private lateinit var agentDescDesc: TextView

    private lateinit var agentAbilities: TextView
    private lateinit var agentAb1Img: ImageView
    private lateinit var agentAb1Text: TextView
    private lateinit var agentAb2Img: ImageView
    private lateinit var agentAb2Text: TextView
    private lateinit var agentAb3Img: ImageView
    private lateinit var agentAb3Text: TextView
    private lateinit var agentUltImg: ImageView
    private lateinit var agentUltText: TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        agentNameDetails = view.findViewById(R.id.agent_name_details)

        agentFullImg = view.findViewById(R.id.agent_full_img)
        agentRoleInf = view.findViewById(R.id.agent_role_inf)
        agentRoleDesc = view.findViewById(R.id.agent_role_description)
        agentDescInf = view.findViewById(R.id.agent_desc_inf)
        agentDescDesc = view.findViewById(R.id.agent_desc_description)

        agentAbilities = view.findViewById(R.id.agent_abilities)
        agentAb1Img = view.findViewById(R.id.agent_ab_1_img)
        agentAb1Text = view.findViewById(R.id.agent_ab_1_text)
        agentAb2Img = view.findViewById(R.id.agent_ab_2_img)
        agentAb2Text = view.findViewById(R.id.agent_ab_2_text)
        agentAb3Img = view.findViewById(R.id.agent_ab_3_img)
        agentAb3Text = view.findViewById(R.id.agent_ab_3_text)
        agentUltImg = view.findViewById(R.id.agent_ult_img)
        agentUltText = view.findViewById(R.id.agent_ult_text)


        agentsViewModel.getAgentDetail(arguments?.getString("uuid")!!)

        Log.d("UUID: ",arguments?.getString("uuid").toString())

        agentsViewModel.agent.observe(viewLifecycleOwner, {
            agentNameDetails.text = it.data.displayName

            Picasso.get().load(it.data.fullPortrait).into(agentFullImg)
            agentRoleDesc.text = it.data.role.description
            agentDescDesc.text = it.data.description

            val abIcon1 = it.data.abilities[0].displayIcon
            val abIcon2 = it.data.abilities[1].displayIcon
            val abIcon3 = it.data.abilities[2].displayIcon
            val ultIcon = it.data.abilities[3].displayIcon

            val abText1 = it.data.abilities[0].displayName
            val abText2 = it.data.abilities[1].displayName
            val abText3 = it.data.abilities[2].displayName
            val ultText = it.data.abilities[3].displayName

            val abDesc1 = it.data.abilities[0].description
            val abDesc2 = it.data.abilities[1].description
            val abDesc3 = it.data.abilities[2].description
            val ultDesc = it.data.abilities[3].description


            Picasso.get().load(abIcon1).into(agentAb1Img)
            agentAb1Text.text = abText1
            agentAb1Img.setOnClickListener {
                abilityDialogStart("Ability 1", abIcon1, abText1, abDesc1)
            }

            Picasso.get().load(abIcon2).into(agentAb2Img)
            agentAb2Text.text = abText2
            agentAb2Img.setOnClickListener {
                abilityDialogStart("Ability 2", abIcon2, abText2, abDesc2)
            }

            Picasso.get().load(abIcon3).into(agentAb3Img)
            agentAb3Text.text = abText3
            agentAb3Img.setOnClickListener {
                abilityDialogStart("Ability 3", abIcon3, abText3, abDesc3)
            }

            Picasso.get().load(ultIcon).into(agentUltImg)
            agentUltText.text = ultText
            agentUltImg.setOnClickListener {
                abilityDialogStart("Ultimate", ultIcon, ultText, ultDesc)
            }
        })


    }

    private fun abilityDialogStart(num: String, img: String, name: String, desc : String) {
        val view = View.inflate(requireContext(), R.layout.agent_abilities_layout, null)

        val builder = AlertDialog.Builder(requireContext())
        builder.setView(view)

        val dialog = builder.create()

        val abNum : TextView = view.findViewById(R.id.abNum)
        val abImg : ImageView = view.findViewById(R.id.abImg)
        val abName : TextView = view.findViewById(R.id.abName)
        val abDescription : TextView = view.findViewById(R.id.abDescription)

        abNum.text = num
        Picasso.get().load(img).into(abImg)
        abName.text = name
        abDescription.text = desc

        dialog.show()

        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

        view.findViewById<View>(R.id.buttonAction).setOnClickListener {
            dialog.dismiss()
        }
    }
}

