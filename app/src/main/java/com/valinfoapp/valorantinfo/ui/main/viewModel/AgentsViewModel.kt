package com.valinfoapp.valorantinfo.ui.main.viewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.valinfoapp.valorantinfo.data.model.AgentEntity
import com.valinfoapp.valorantinfo.data.model.DetailData
import com.valinfoapp.valorantinfo.data.model.ListData
import com.valinfoapp.valorantinfo.data.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import androidx.lifecycle.LiveData

class AgentsViewModel(): ViewModel() {

    private var repository = Repository()
    val agents = MutableLiveData<ListData>()
    var agent = MutableLiveData<DetailData>()


    fun getAgentsDB(): LiveData<MutableList<AgentEntity>> {
        val itemList = repository.mItemList
        return itemList
    }

    fun getAgentsList() {
        viewModelScope.launch {
            val call = withContext(Dispatchers.IO) { repository.getAgentsList() }
            call.enqueue(object: Callback<ListData>{
                override fun onResponse(
                    call: Call<ListData>,
                    response: Response<ListData>
                ) {
                    Log.d("LIST RESPONSE", "GET Body : ${response.body()} ")
                    agents.postValue(response.body())
                }

                override fun onFailure(call: Call<ListData>, t: Throwable) {
                    Log.d("LIST FAILURE", t.message.toString())
                }
            })
        }
    }

    fun getAgentDetail(agentUuid: String) {
        viewModelScope.launch {
            val call = withContext(Dispatchers.IO) { repository.getAgentDetail(agentUuid) }
            call.enqueue(object : Callback<DetailData> {
                override fun onResponse(call: Call<DetailData>, response: Response<DetailData>) {
                    Log.d("DATA RESPONSE", "GET Body : ${response.body()} ")
                    agent.postValue(response.body())
                }

                override fun onFailure(call: Call<DetailData>, t: Throwable) {
                    Log.d("DATA FAILURE", t.message.toString())
                }
            })
        }
    }
}

