package com.valinfoapp.valorantinfo.ui.main.adapter

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.valinfoapp.valorantinfo.AgentApplication
import com.valinfoapp.valorantinfo.R
import com.valinfoapp.valorantinfo.data.model.AgentEntity
import com.valinfoapp.valorantinfo.data.model.Role
import com.valinfoapp.valorantinfo.ui.main.view.AgentFavouriteFragmentDirections
import com.valinfoapp.valorantinfo.ui.main.view.AgentSearchFragmentDirections
import com.valinfoapp.valorantinfo.ui.main.view.AgentsListFragmentDirections
import com.squareup.picasso.Picasso
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class AgentsAdapter : RecyclerView.Adapter<AgentsAdapter.AgentsViewHolder>() {

    private var agents = mutableListOf<AgentEntity>()


    @SuppressLint("NotifyDataSetChanged")
    fun setAgentsList(agentEntityDBS: MutableList<AgentEntity>) {
        for (i in agentEntityDBS) {
            if (agents.size < agentEntityDBS.size) {
                agents.add(i)
                Log.d("Agents: ", agents.size.toString())
            }
        }

        //A la API hi ha dos "Agents" repetits, hi ha un que dona error, per això temporalment l'omet de la llista.
        agents =
            agents.filter { it.uuid != "ded3520f-4264-bfed-162d-b080e2abccf9" } as MutableList<AgentEntity>
        notifyDataSetChanged()

        Log.d("Agents Final: ", agents.size.toString())
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setAgentBySearch(agentEntityDBS: MutableList<AgentEntity>, searchMame: String) {
        for (i in agentEntityDBS) {
            if (agents.size < agentEntityDBS.size) {
                if (i.displayName.lowercase() == searchMame) {
                    agents.add(i)
                    if (agents.size > 1) {
                        agents.removeAt(0)
                    }
                }
                Log.d("Agents: ", agents.size.toString())
            }
        }
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setAgentFav(agentEntityDBS: MutableList<AgentEntity>) {
        Log.d("Fav: ", agents.size.toString())
        agents.clear()

        for (i in agentEntityDBS) {
            if (agents.size < agentEntityDBS.size) {
                agents.add(i)
                Log.d("Agents: ", agents.size.toString())
            }
        }

        //A la API hi ha dos "Agents" repetits, hi ha un que dona error, per això temporalment l'omet de la llista.
        agents =
            agents.filter { it.uuid != "ded3520f-4264-bfed-162d-b080e2abccf9" } as MutableList<AgentEntity>
            agents = agents.filter { it.fav } as MutableList<AgentEntity>
        notifyDataSetChanged()
    }

    fun removeProduct(model: AgentEntity) {
        val position = agents.indexOf(model)
        agents.remove(model)
        notifyItemRemoved(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AgentsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.agent_item, parent, false)
        return AgentsViewHolder(view)
    }

    override fun onBindViewHolder(holder: AgentsViewHolder, position: Int) {
        holder.bindData(agents[position])
    }

    override fun getItemCount(): Int {
        return agents.size
    }

    class AgentsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var agentIcon: ImageView = itemView.findViewById(R.id.agent_icon)
        private var agentName: TextView = itemView.findViewById(R.id.agent_name)
        private var agentRole: TextView = itemView.findViewById(R.id.agent_role)
        private var favButton: ImageView = itemView.findViewById(R.id.fav_button)



        @SuppressLint("SetTextI18n", "UseCompatLoadingForDrawables")
        fun bindData(AgentEntity: AgentEntity) {

            val favIcon = R.drawable.favourite_icon
            val favIconSel = R.drawable.favourite_sel_icon

            Picasso.get().load(AgentEntity.displayIcon).into(agentIcon)
            agentName.text = AgentEntity.displayName
            agentRole.text = " - " + AgentEntity.role.displayName

            if (AgentEntity.fav) {
                favButton.setBackgroundResource(favIconSel)
            } else if (!AgentEntity.fav) {
                favButton.setBackgroundResource(favIcon)
            }

            favButton.setOnClickListener {
                CoroutineScope(Dispatchers.IO).launch {
                    if (!AgentEntity.fav) {
                        favButton.setBackgroundResource(favIconSel)
                        AgentEntity.fav = true

                        val uuid = AgentEntity.uuid
                        val displayName = AgentEntity.displayName
                        val description = AgentEntity.description
                        val displayIcon = AgentEntity.displayIcon
                        val fullPortrait = AgentEntity.fullPortrait
                        val role: Role = AgentEntity.role
                        val abilities = AgentEntity.abilities
                        val fav = AgentEntity.fav

                        val newAgent = AgentEntity(
                            uuid = uuid,
                            displayName = displayName,
                            description = description,
                            displayIcon = displayIcon,
                            fullPortrait = fullPortrait,
                            role = role,
                            abilities = abilities,
                            fav = fav
                        )
                        AgentApplication.database.agentDao().updateAgent(newAgent)


                    } else if (AgentEntity.fav) {
                        favButton.setBackgroundResource(favIcon)
                        AgentEntity.fav = false

                        val uuid = AgentEntity.uuid
                        val displayName = AgentEntity.displayName
                        val description = AgentEntity.description
                        val displayIcon = AgentEntity.displayIcon
                        val fullPortrait = AgentEntity.fullPortrait
                        val role: Role = AgentEntity.role
                        val abilities = AgentEntity.abilities
                        val fav = AgentEntity.fav

                        val newAgent = AgentEntity(
                            uuid = uuid,
                            displayName = displayName,
                            description = description,
                            displayIcon = displayIcon,
                            fullPortrait = fullPortrait,
                            role = role,
                            abilities = abilities,
                            fav = fav
                        )
                        AgentApplication.database.agentDao().updateAgent(newAgent)
                    }
                }
            }

            itemView.setOnClickListener {
                try {
                    val direction =
                        AgentsListFragmentDirections.actionAgentsListFragment2ToAgentDetailsFragment2(AgentEntity.uuid)
                    Navigation.findNavController(itemView).navigate(direction)
                } catch (e : Exception) {
                    try {
                        val direction =
                            AgentSearchFragmentDirections.actionAgentSearchFragment2ToAgentDetailsFragment2(AgentEntity.uuid)
                        Navigation.findNavController(itemView).navigate(direction)
                    } catch (e: java.lang.Exception) {
                        val direction =
                            AgentFavouriteFragmentDirections.actionAgentFavouriteFragment2ToAgentDetailsFragment2(AgentEntity.uuid)
                        Navigation.findNavController(itemView).navigate(direction)
                    }
                }
            }
        }
    }
}

