package com.valinfoapp.valorantinfo.ui.main.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.valinfoapp.valorantinfo.R
import com.valinfoapp.valorantinfo.ui.main.adapter.AgentsAdapter
import com.valinfoapp.valorantinfo.ui.main.viewModel.AgentsViewModel

class AgentFavouriteFragment: Fragment(R.layout.agents_fav_layout) {
    private lateinit var valTitleLogo: ImageView
    private lateinit var allList: TextView
    private lateinit var searchAgent: TextView
    private lateinit var favAgent: TextView

    private lateinit var agentsRecyclerView: RecyclerView
    private val agentsViewModel: AgentsViewModel by activityViewModels()
    private val adapter = AgentsAdapter()

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        valTitleLogo = view.findViewById(R.id.val_title_logo)
        allList = view.findViewById(R.id.all_list)
        searchAgent = view.findViewById(R.id.search_agent)
        favAgent = view.findViewById(R.id.fav_agent)


        allList.setOnClickListener(){
            val direction =  AgentFavouriteFragmentDirections.actionAgentFavouriteFragment2ToAgentsListFragment2()
            Navigation.findNavController(view).navigate(direction)
        }

        searchAgent.setOnClickListener(){
            val direction = AgentFavouriteFragmentDirections.actionAgentFavouriteFragment2ToAgentSearchFragment2()
            Navigation.findNavController(view).navigate(direction)
        }

        agentsRecyclerView = view.findViewById(R.id.agents_recycle_view)

        agentsRecyclerView.layoutManager = LinearLayoutManager(context)

        agentsRecyclerView.adapter = adapter


        agentsViewModel.getAgentsDB().observe(viewLifecycleOwner, Observer { item ->
            // Update the UI
            item.let { adapter.setAgentFav(it!!) }
        })
    }
}