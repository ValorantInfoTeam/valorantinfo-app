package com.valinfoapp.valorantinfo.ui.main.view

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.valinfoapp.valorantinfo.AgentApplication
import com.valinfoapp.valorantinfo.R
import com.valinfoapp.valorantinfo.data.model.AgentEntity
import com.valinfoapp.valorantinfo.data.model.Role
import com.valinfoapp.valorantinfo.ui.main.viewModel.AgentsViewModel
import com.squareup.picasso.Picasso
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MenuFragment : Fragment(R.layout.menu_layout) {

    private lateinit var valTitleLogo: ImageView
    private lateinit var appSlogan: TextView
    private lateinit var imageSwitcherAg: ImageView
    private lateinit var agentNameMenu: TextView
    private lateinit var agentsInfButton: Button
    private val agentsViewModel: AgentsViewModel by activityViewModels()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        valTitleLogo = view.findViewById(R.id.val_title_logo)
        appSlogan = view.findViewById(R.id.app_slogan)
        imageSwitcherAg = view.findViewById(R.id.imageSwitcher_ag_wp)
        agentNameMenu = view.findViewById(R.id.agent_name_menu)
        agentsInfButton = view.findViewById(R.id.agents_inf_button)

        agentsViewModel.getAgentsList()

        agentsViewModel.agents.observe(viewLifecycleOwner, {

            var agents1 = mutableListOf<AgentEntity>()

            for (i in it.data) {
                if (agents1.size < it.data.size) {
                    agents1.add(i)
                    Log.d("Agents: ", agents1.size.toString())
                }
            }

            agents1 =
                agents1.filter { it.uuid != "ded3520f-4264-bfed-162d-b080e2abccf9" } as MutableList<AgentEntity>
            Picasso.get().load(agents1[0].fullPortrait).into(imageSwitcherAg)
            agentNameMenu.text = agents1[0].displayName
            val handler = Handler()
            val runnable: Runnable = object : Runnable {
                var i = 1
                override fun run() {
                    Picasso.get().load(agents1[i].fullPortrait).into(imageSwitcherAg)
                    agentNameMenu.text = agents1[i].displayName
                    i++
                    if (i > agents1.size - 1) {
                        i = 0
                    }
                    handler.postDelayed(this, 3500)
                }
            }
            handler.postDelayed(runnable, 3500)

            CoroutineScope(Dispatchers.IO).launch {

                var agents = it.data

                agents =
                    agents.filter { it.uuid != "ded3520f-4264-bfed-162d-b080e2abccf9" } as MutableList<AgentEntity>

                for (i in agents) {
                    val uuid = i.uuid
                    val displayName = i.displayName
                    val description = i.description
                    val displayIcon = i.displayIcon
                    val fullPortrait = i.fullPortrait
                    val role: Role = i.role
                    val abilities = i.abilities
                    val fav = i.fav

                    val newContact = AgentEntity(
                        uuid = uuid,
                        displayName = displayName,
                        description = description,
                        displayIcon = displayIcon,
                        fullPortrait = fullPortrait,
                        role = role,
                        abilities = abilities,
                        fav = fav
                    )
                    AgentApplication.database.agentDao().addAgent(newContact)
                }
            }
        })

        agentsInfButton.setOnClickListener {
            findNavController().navigate(R.id.action_menuFragment2_to_agentsListFragment2)
        }
    }
}